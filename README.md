# README #

This README is about a Bomberman game as programming test made with Unreal

## Current version ##
v0.3 (single player with movement, bombs, main menu, hud and char ai controller, networking skeleton programming structure added )
 
### What is this repository for? ###

The goal is to create a multiplayer "programmer-art" version of the old Dyna Blaster/Bomberman game in 3D.
Develop the game in incremental versions and tag these as releases in the Git repository:

* Single player version with bombs, destructible walls, and pickups
* Multiplayer version over local network or Internet
* Enemy AI that controls other characters until actual players join

### How do I get set up? ###

* Install Visual Studio 2017 and Unreal 4.17.2
* Open the UEBomberman project
* Compile and open editor
* Hit Play


### Tasks Done ###

* Static top-down view on the level
* Basic level pre-defined
* Character:
	- movement
	- drop bombs
	- Added a character AI Controller to be used when there is no player to play
* Bombs:
	- instantiate when player hit Fire button 
	- initial explossion after time
	- sound 
	- blast length depending on the current player bomb power
* Blocks:
	- C++ Base Class 
	- BP for breakeable wall blocks with random powerup generation
	- BP for non-breakeable wall blocks
* Power Ups:
	- C++ Base class + 3 blueprints (bombs count,char speed,blast length)
* Enemies:
	- Single class + blueprint + model + texture
	- Generated from BP: Boar (inherits from AEnemy class)
	- TODO: add movement and configure colliders/events
* Levels:
  + procedural constructor
	- Having an array of elements, will create a level with :
		- blocks (breakeable and non-breakeable with & without powerups)
		- enemies (not ai available yet to move them)
		- starts for characters (spawn points)
		- exit
		- todo: read the levels from file / database
  + other levels:
	- Lobby with character selection
	- TODO: Config screen, End screen with results 
  + camera:
	- TODO: use an arm to pan the view depending on chars locations
* Single
* HUD:
	- Main Menu
		- todo: add buttons to enter multiplayer and config screens
	- TODO: 
		- Add menu for network options: create, join (like this: https://www.youtube.com/watch?v=_xVrypniFKU done by me)
		- add menu for ingame
		- add hud for player -character- information (stats)
		- add hud for map (using game mode and game state classes): show map timer
		- results of the game: win,lose,etc.
		- huds for network players (name,stats...)
* Networking: 
	- C++ Unreal multiplayer sessions class attached to game mode for that purpose
	- characters can be replicated 
	
### Brief Explanation of the programmer-art test making ###
The first thing I wanted to do was to organize the game the way 
I could scale it not depending on what global feature you may want to change:
singleplayer, AI or multiplayer. 
So, I decided to use two modes to play: 
singleplayer and multiplayer, with a simple C++ base for all.
The mechanics of the game: 
a player character ,a bomb, enemy, blocks,etc. all inside a generated level.
I use these simple classes to build configuratble BPs for the designers 
to choose options and to replicate the actors in network sessions.
At the same time, the character can be used with an ai controller instead of 
the user input.
The idea is to use the Unreal Networking but we could use PhotonCloud with an 
implementation I already did, if we want to use a cloud network, faster and 
reliable than a peer2peer one.
I used some of the UML pattern designs, and KISS principle.


# Will I Continue with Project?
I already did this game for Unity, that's where I got my assets 
and some of the ideas ,but this time, with networking sounds even
more interesting, so, why not doing a VR version of this bomberman?!