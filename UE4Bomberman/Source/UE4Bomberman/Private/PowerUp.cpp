#include "PowerUp.h"


// Sets default values
APowerUp::APowerUp(const FObjectInitializer& ObjectInitializer)
{
	DefaultSceneRoot = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("DefaultSceneRoot"));
	DefaultSceneRoot->SetMobility(EComponentMobility::Movable);

	// Set RootComponent if no root
	if (!RootComponent)
		RootComponent = DefaultSceneRoot;
	PrimaryActorTick.bCanEverTick = true;

	//Mesh
	powerUpMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("PowerUpMesh"));
	powerUpMesh->SetRelativeTransform(FTransform(
		FRotator(0.0f, 0.0f, 0.0f),
		FVector(0.0f, 0.0f, 0.0f),
		FVector(1.0f, 1.0f, 1.0f))
	);
	powerUpMesh->SetMobility(EComponentMobility::Movable);
	powerUpMesh->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);


	//Audio
	AudioComponent = ObjectInitializer.CreateDefaultSubobject<UAudioComponent>(this, TEXT("AudioComponent"));
	AudioComponent->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);
	AudioComponent->bAutoActivate = false;

	//deactivate collision at the start
	SetActorEnableCollision(false);
	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
}

// Called when the game starts or when spawned
void APowerUp::BeginPlay()
{
	Super::BeginPlay();
	
}

void APowerUp::ShowUp()
{
	hidden = false;
	SetActorTickEnabled(true);
	SetActorEnableCollision(true);
	SetActorHiddenInGame(false);
}

void APowerUp::OnPicked()
{
	//play sound

	//do animation

	//delete me after animation
}

// Called every frame
void APowerUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (hidden) return;
	if (!picked) 
	{
		timeToHideAfterShowed -= DeltaTime;
		if (timeToHideAfterShowed<=0.0f) {
			//Delete me
			Destroy();
		} else {
			//rotate
			AddActorLocalRotation(FRotator(0.0f,rotationSpeed,0.0f)*DeltaTime);
		}
	}
}

