
#include "Bomb.h"
#include "BombermanCharacter.h"
#include "UE4Bomberman.h"
#include "Block.h"


// Sets default values
ABomb::ABomb(const FObjectInitializer& ObjectInitializer) : bombMesh(nullptr), fuse_fire_component_(nullptr)
{
	DefaultSceneRoot = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("DefaultSceneRoot"));
	DefaultSceneRoot->SetMobility(EComponentMobility::Movable);

	// Set RootComponent if no root
	if (!RootComponent)
		RootComponent = DefaultSceneRoot;
	PrimaryActorTick.bCanEverTick = false;

	//Mesh
	bombMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("BombMesh"));
	bombMesh->SetRelativeTransform(FTransform(
		FRotator(0.0f, 0.0f, 0.0f),
		FVector(0.0f, 0.0f, 0.0f),
		FVector(1.0f, 1.0f, 1.0f))
	);
	bombMesh->SetMobility(EComponentMobility::Movable);
	bombMesh->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);


	//Audio
	AudioComponent = ObjectInitializer.CreateDefaultSubobject<UAudioComponent>(this, TEXT("AudioComponent"));
	AudioComponent->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);
	AudioComponent->bAutoActivate = false;

	//deactivate collision at the start
	SetActorEnableCollision(false);
}

// Called when the game starts or when spawned
void ABomb::BeginPlay()
{
	Super::BeginPlay();
}

void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABomb::HeartBeat()
{
	if (!activated) return;
	if (!exploded)
	{
		if (!exploding) {
			//Size animation
			bombMesh->SetRelativeScale3D(
				FVector(
					bombMesh->RelativeScale3D.X + sizeSpeed,
					bombMesh->RelativeScale3D.Y + sizeSpeed,
					bombMesh->RelativeScale3D.Z + sizeSpeed
				)
			);
			//set the new scale for animation
			if (bombMesh->RelativeScale3D.X < 0.79f) {
				sizeSpeed = 0.007;
			}
			else if (bombMesh->RelativeScale3D.X > 1.1f) {
				sizeSpeed = -0.007;
			}

			//exploding 
			fuseTime -= heartbeatTickTime;
			if (fuseTime <= 0.0f)
			{
				Explode();
			}
			else
			{
				if (!GetActorEnableCollision() && fuseTime >= timeToEnableCollisionAfterDrop)
				{
					SetActorEnableCollision(true);
				}
			}
		}
	}
}

float ABomb::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if(ActualDamage>0.0f)
	{
		Explode();
	}
	return ActualDamage;
}


void ABomb::Activate(ABombermanCharacter* bombermanOwner, int bombPower, float timeToExplode)
{
	character_owner = bombermanOwner;
	power = bombPower;
	fuseTime = timeToExplode;
	//create fire particles for the fuse
	if (ps_bombFuseFire) {
		fuse_fire_component_ = UGameplayStatics::SpawnEmitterAttached(
			ps_bombFuseFire, RootComponent, NAME_None,
			GetActorLocation(), FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition
		);
		if (fuse_fire_component_) {
			fuse_fire_component_->SetRelativeLocation(fuse_fire_offset);
			fuse_fire_component_->SetRelativeScale3D(fuse_fire_size);
		}
	}

	//everything set, activate bomb
	activated = true;
	//program timer to explode
	GetWorld()->GetTimerManager().SetTimer(FuseTimerHandle, this, &ABomb::Explode, fuseTime, false);
	//program repetitive timer to animate size
	GetWorld()->GetTimerManager().SetTimer(HeartBeatTimerHandle, this, &ABomb::HeartBeat, heartbeatTickTime, true);
}




void ABomb::Explode()
{
	if (exploded || exploding) return; //aready exploded?

	//cancel timer for size animation
	if (HeartBeatTimerHandle.IsValid())
		GetWorld()->GetTimerManager().ClearTimer(HeartBeatTimerHandle);

	//cancel timer to explode in the case the bomb explode from other event 
	if (FuseTimerHandle.IsValid())
		GetWorld()->GetTimerManager().ClearTimer(FuseTimerHandle);

	//mark bomb as exploding already
	exploding = true;
	//bomb explosion fx
	if (ps_bombInitialExplosion)
	{
		auto initialExplosion = UGameplayStatics::SpawnEmitterAtLocation(
			this->GetWorld(), ps_bombInitialExplosion, GetActorLocation(),
			FRotator::ZeroRotator,
			true
		);
	}

	//play sound
	AudioComponent->Activate(true);
	AudioComponent->Play(0.0f);

	//hide mesh
	bombMesh->SetVisibility(false);

	//hide fuse
	if (fuse_fire_component_) {
		fuse_fire_component_->Deactivate();
	}
	

	//spawn fire particle fxs along the rows and columns depending on the fire power and calculate the explosion time
	
	if (ps_bombBlastFire) 
	{
		const FVector &bombPosition = GetActorLocation();
		const static float fireDirections[4][2] = { {1.0f,0.0f },{-1.0f,0.0f},{0.0f,1.0f },{0.0f,-1.0f } };
		UWorld *world = GetWorld();
		TArray<TEnumAsByte<EObjectTypeQuery> > CollisionObjectTypes;
		//CollisionObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery1);
		TArray<AActor*> ignoreActors;
		for (int j = 0; j < 4; j++) {
			for (int i = 0; i < power; i++)
			{
				FVector blastPosition = bombPosition + FVector((i+1)*fireDirections[j][0], (i+1)*fireDirections[j][1],0.0f);
				//create one more blast part
				auto blast = UGameplayStatics::SpawnEmitterAttached(
					ps_bombBlastFire, RootComponent, NAME_None,
					blastPosition,
					FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition,
					true
				);
				fuse_fire_blast_components_.Add(blast);
				TArray<AActor*> collidingActors;
				if (UKismetSystemLibrary::SphereOverlapActors(world, blastPosition, 1.0f, CollisionObjectTypes, nullptr, ignoreActors, collidingActors))
				{
					bool stopPropagating = false;
					for (int32 k = 0; k < collidingActors.Num(); k++)
					{
						//Notice my owner i have hit something with one of my blast
						if (character_owner) {
							character_owner->OnMyBombHitActor(this, collidingActors[k]);
						} else {
							//call the object damage itself
							//collidingActors[i]->TakeDamage() //@todo
						}
						//is it a block?
						ABlock *isBlock = Cast<ABlock>(collidingActors[i]);
						if (isBlock) {
							//stop blast in this direction
							stopPropagating = true;
							break;
						}
					}
					if (stopPropagating) break;
				}
				

				
			}
		}
		
	}
	explosionBlastTime = (fuse_fire_blast_components_.Num()/4)*timePerExplosion;
	//bomb exploding already
	exploding = true;

	//program a timer to finish with all the explossion process in range
	GetWorld()->GetTimerManager().SetTimer(BlastTimerHandle, this, &ABomb::OnBlastsFinished, explosionBlastTime, false);
}

bool ABomb::Exploded()
{
	return exploded;
}

void ABomb::OnBlastsFinished()
{
	//cancel blast timer if the function is called from other origin than the timer handle
	if (BlastTimerHandle.IsValid())
		GetWorld()->GetTimerManager().ClearTimer(BlastTimerHandle);
	
	//disable all blasts
	for (int i = 0; i<fuse_fire_blast_components_.Num(); i++)
	{
		fuse_fire_blast_components_[i]->DestroyComponent();
	}

	exploding = false;
	exploded = true;
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
	SetActorTickEnabled(false);
	
	//notice the owner
	if(character_owner)
	{
		character_owner->OnMyBombExploded(this);
	}
	else
	{
		//TODO: apply damage in this case?

		//bomber has no owner, just destroy me
		Destroy(true);
		
	}
}

