

#include "Block.h"
#include "BombBlastDamage.h"


// Sets default values
ABlock::ABlock(const FObjectInitializer& ObjectInitializer)
{
	DefaultSceneRoot = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("DefaultSceneRoot"));
	DefaultSceneRoot->SetMobility(EComponentMobility::Static);

	// Set RootComponent if no root
	if (!RootComponent)
		RootComponent = DefaultSceneRoot;
	PrimaryActorTick.bCanEverTick = false;

	//Mesh
	blockMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("BlockMesh"));
	blockMesh->SetRelativeTransform(FTransform(
		FRotator(0.0f, 0.0f, 0.0f),
		FVector(0.0f, 0.0f, 0.0f),
		FVector(1.0f, 1.0f, 1.0f))
	);
	blockMesh->SetMobility(EComponentMobility::Static);
	blockMesh->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);


	//Audio
	AudioComponent = ObjectInitializer.CreateDefaultSubobject<UAudioComponent>(this, TEXT("AudioComponent"));
	AudioComponent->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);
	AudioComponent->bAutoActivate = false;

	//deactivate collision at the start
	SetActorEnableCollision(true);
}


void ABlock::BeginPlay()
{
	Super::BeginPlay();
}

void ABlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float ABlock::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if(ActualDamage>0.0f)
	{
		UBombBlastDamage *damage = Cast<UBombBlastDamage>(DamageEvent.DamageTypeClass);
		if (damage != nullptr) {
			OnReceiveHit(GetActorLocation() + FVector::UpVector*3.0f, damage->DestructibleImpulse);
		}
	}
	return ActualDamage;
}

