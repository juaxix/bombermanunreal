// Juan Belon - 2017
#include "BombermanPlayerController.h"
#include "BombermanCharacter.h"


ABombermanPlayerController::ABombermanPlayerController()
{
	
	
}

void ABombermanPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (!InputComponent) return;
	
	//check the type of pawn:
	ABombermanCharacter *bomberman_character = Cast<ABombermanCharacter>(GetPawn());
	if(bomberman_character!=nullptr && IsValid(bomberman_character) && InputComponent)
	{
		//set up input
		InputComponent->BindAxis("MoveForward", bomberman_character, &ABombermanCharacter::MoveForward);
		InputComponent->BindAxis("MoveRight", bomberman_character, &ABombermanCharacter::MoveRight);
		InputComponent->BindAction("Fire", IE_Pressed, bomberman_character, &ABombermanCharacter::Fire);
	}
}
