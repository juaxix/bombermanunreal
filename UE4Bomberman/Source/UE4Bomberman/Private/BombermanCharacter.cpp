// code: Juan Bel�n


#include "BombermanCharacter.h"
#include "BombBlastDamage.h"
#include "Block.h"


// Sets default values
ABombermanCharacter::ABombermanCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void ABombermanCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABombermanCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void ABombermanCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//@TODO: add replicated behaviour
	//if (!network_replicatedActor) {
	//	
	//}
	
}

float ABombermanCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (totalHitPoints <= 0) return 0.0f; //cant hit me if i'm dead
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if(ActualDamage>0.0f)
	{
		totalHitPoints--;
		if(totalHitPoints<=0)
		{
			//Dead()
		}
	}
	return ActualDamage;
}

bool ABombermanCharacter::BombsAvailable()
{
	return dropped_bombs.Num() < maxBombs;
}

void ABombermanCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
}

void ABombermanCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(FVector::ForwardVector, AxisValue*speed, false);
	float axisRight = this->Controller->InputComponent->GetAxisValue("MoveRight");
	if (AxisValue != 0.0f || axisRight!=0.0f)
	{
		SetActorRotation(FRotator(0.f, FMath::RadiansToDegrees(FMath::Atan2(axisRight, AxisValue)), 0.f), ETeleportType::None );
	}
}

void ABombermanCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(FVector::RightVector, AxisValue*speed, false);
}

void ABombermanCharacter::CollideWithPowerUp(APowerUp* powerup)
{
	if(powerup==nullptr)
	{
		//something went very wrong :/
		UE_LOG(LogTemp, Error, TEXT("Character %s got an invalid powerup"), *GetName());
		return;
	}
	/** @TODO */
	//depending on the type of power up we do different things 
	//we can use a delegate function that changes a set of properties
	//or use an UML pattern like a Command to operate with maths

	//....


	//at the end we call the blueprint function to be implemented by the user (bp programmer?)
	OnPickPowerUp(powerup);
}

void ABombermanCharacter::OnMyBombExploded(ABomb* bomb)
{
	//Check bomb
	if (bomb == nullptr || !bomb->IsValidLowLevel() || bomb->IsPendingKillPending()) return;

	//get actors affected by the bomb blast/explosion
	const TArray<AActor*> actors = bomb->GetDamagedActors();

	//check that all actors affected by this bomb explosion blast was damaged already by OnMyBombHitActor event
	//...
	
	//remove bomb from my list of dropped bombs
	dropped_bombs.Remove(bomb);
	//remove bomb from memory
	bomb->Destroy(true);
}

/** @TODO: Change this function to use with an Interface to make bomb damage to actors? or leave with takedamage */
void ABombermanCharacter::OnMyBombHitActor(ABomb *bomb, AActor* actor)
{
	if (!actor || !bomb || !IsValid(actor) || !IsValid(bomb) || !actor->bCanBeDamaged) {
		UE_LOG(LogTemp, Display, TEXT("Error with the bomb or actor ,or actor can not be damaged by bomb"));
		return;
	}

	FPointDamageEvent damageEvent;
	const FVector direction((bomb->GetActorLocation() - actor->GetActorLocation()).Normalize());
	damageEvent.ShotDirection = FVector_NetQuantize(direction.X, direction.Y, direction.Z);
	damageEvent.Damage = 1.0f;
	damageEvent.DamageTypeClass = UBombBlastDamage::StaticClass();
	UBombBlastDamage *BlastDamage = Cast<UBombBlastDamage>(damageEvent.DamageTypeClass->GetDefaultObject());
	BlastDamage->DamageFalloff = 1.2f;
	BlastDamage->DamageImpulse = 600.0f;
	BlastDamage->DestructibleDamageSpreadScale = 1000.0f;
	BlastDamage->DestructibleImpulse = 600.0f;
	/** @TODO: associate BlastDamage to damageEvent */
	actor->TakeDamage(1.0f, damageEvent, nullptr, bomb);
}

void ABombermanCharacter::Fire()
{
	//create bomb using the template class in the blueprint or fallback to a basic bomb
	UClass* bombClazz = bombType != nullptr ? bombType : ABomb::StaticClass();
	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	FVector bombLocation = GetActorLocation();
	bombLocation.Z = 0.0f; // @TODO calculate the height depending on the floor with ray
	AActor* bombActor = GetWorld()->SpawnActor(bombClazz, &bombLocation, &FRotator::ZeroRotator,params);
	if (bombActor) {
		ABomb *bomb = Cast<ABomb>(bombActor);
		if (bomb) {
			//configure bomb and activate it:
			bomb->Activate(this, bombPower, bombExplosionTimeout);
			dropped_bombs.Add(Cast<ABomb>(bombActor));
		} else
		{
			UE_LOG(LogTemp, Warning, TEXT("Could not use the bomb with class %s, it's not a bomb!"), *bombClazz->GetName());
		}
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not create the bomb with class %s!"), *bombClazz->GetName());
	}
}
