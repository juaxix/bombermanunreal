#include "LevelGenerator.h"
#include "Enemy.h"

// Sets default values for this component's properties
ULevelGenerator::ULevelGenerator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
		
	if (readLevelFromFile) {
		uint32 Rows=0, Columns=0;
		for (uint32 Row = 0; Row < Rows; Row++)
		{
			TArray<int> Temp;
			Temp.Init(NULL, Columns);
			level_array.Add(Temp);
		}
		// @TODO: read level map here from a file
	} else {
		#define TEMP_ROWS 11
		#define TEMP_COLS 13
		
		int _level_array[TEMP_ROWS][13] =
		{
			{ 3, 0, 0, 1, 0, 0, 10, 0, 0, 0, 0, 0, 0 },
			{ 0, 2, 1, 2, 1, 2, 1, 2, 1, 2, 21, 2, 0 },
			{ 0, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 1, 0 },
			{ 1, 2, 0, 2, 1, 2, 1, 2, 1, 2, 0, 2, 0 },
			{ 0, 1, 0, 1, 0, 0, 10, 0, 0, 1, 0, 1, 0 },
			{ 0, 2, 0, 2, 0, 2, 1, 2, 0, 2, 0, 2, 0 },
			{ 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0 },
			{ 1, 2, 0, 2, 1, 2, 1, 2, 1, 2, 0, 2, 0 },
			{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
			{ 0, 2, 4, 2, 1, 2, 1, 2, 1, 2, 1, 2, 0 },
			{ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		};
		for (uint32 Row = 0; Row < TEMP_ROWS; Row++)
		{
			TArray<int> Temp;
			Temp.Append(_level_array[Row], TEMP_ROWS);
			level_array.Add(Temp);
		}
	}
}

void ULevelGenerator::InitLevel()
{
	if(!breakeableType ||!nonbreakeableType || !enemyType)
	{
		UE_LOG(LogTemp, Error, TEXT("Configure all the classes in the blueprint!"));
		return;
	}
	UWorld *world = GetWorld();
	FActorSpawnParameters spawnParameters;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	for(int i=0;i<level_array.Num();i++)
	{
		for(int j=0;j<level_array[i].Num();j++)
		{
			FVector cell_position = FVector(
				initialCellPositionX + (i*cellSeparation),
				initialCellPositionY + (j*cellSeparation),
				initialCellPositionZ
			);
			switch(level_array[i][j])
			{
			case 0: //Empty cell
				break;
			case 1: {//Breakeable block object
					AActor* aBreak = GetWorld()->SpawnActor(breakeableType, &cell_position, &FRotator::ZeroRotator, spawnParameters);
					if (aBreak)
					{
						//configure
						
						//30% probability of generating a powerup
						if (FMath::RandRange(1, 100) <= 30)
						{
							//aNoBreak->generate_random
						}
					}
					else
					{
						UE_LOG(LogTemp, Error, 
							TEXT("Could not spawn a breakeable object at %f,%d: type %s in %s!"),
							*breakeableType->GetName(), *cell_position.ToString()
						);
					}
				}
				break;
			case 2: {//Unbreakeable object
					AActor* aNoBreak = GetWorld()->SpawnActor(nonbreakeableType, &cell_position, &FRotator::ZeroRotator, spawnParameters);
					if (aNoBreak)
					{
						//configure it

					}
					else
					{
						UE_LOG(LogTemp, Error, TEXT("Could not spawn a %s in %s!"), *breakeableType->GetName(), *cell_position.ToString());
					}
				}
				break;
			case 3:{//Start position
				UGameplayStatics::GetPlayerController(world, 0)->GetPawn()->SetActorLocation(cell_position);
				}
				break;
			case 4: {//End Position
					//door to exit, hidden under a breakeable object
					//@TODO: add model for the door with the colliders
				}
				break;
			case 10: case 11: case 12: case 13: case 14: case 15: //enemies
				{ 
					if(enemyType!=nullptr)
					{
						AActor* aEnemy = GetWorld()->SpawnActor(enemyType, &cell_position, &FRotator::ZeroRotator, spawnParameters);
						if (aEnemy)
						{
							//check enemy
							AEnemy* enemy = Cast<AEnemy>(aEnemy);
							if(enemy)
							{
								//configure it
								//... @TODO: make this enemy to wander , and attack?
							} else
							{
								UE_LOG(LogTemp, Error, TEXT("This actor is not an Enemy %s in %s!"), *enemyType->GetName(), *cell_position.ToString());
							}
						}
						else
						{
							UE_LOG(LogTemp, Error, TEXT("Could not spawn an enemy %s in %s!"), *enemyType->GetName(), *cell_position.ToString());
						}
					}
					//*TODO : spawn more enemies making them walk along the adjacents positions
				}
			}
		}
	}
}


// Called when the game starts
void ULevelGenerator::BeginPlay()
{
	Super::BeginPlay();

	// with the level data in memory, go to init it
	InitLevel();
}


// Called every frame
void ULevelGenerator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

