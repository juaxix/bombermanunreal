// Juan Belon - 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4BombermanGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4BOMBERMAN_API AUE4BombermanGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	
	/** @TODO: Add the logic to use the game state class */
	//this class will determine the base gameplay logic of the game
};
