// Juan Belon - 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BombermanPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class UE4BOMBERMAN_API ABombermanPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ABombermanPlayerController();
	void BeginPlay()
	{
		UE_LOG(LogTemp, Display, TEXT("Initializing player controller"));
		SetupInputComponent();
	}
	void SetupInputComponent() override;
};
