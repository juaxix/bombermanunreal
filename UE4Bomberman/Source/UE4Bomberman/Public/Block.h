// Juan Belon - 2017

#pragma once

#include "Engine.h"
#include "GameFramework/Actor.h"
#include "Block.generated.h"

UCLASS()
class UE4BOMBERMAN_API ABlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlock(const FObjectInitializer& ObjectInitializer);

protected:
	/**
	* Root component of the Block
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	USceneComponent* DefaultSceneRoot;

	/**
	* Contains the mesh and material of this block
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* blockMesh;

	/**
	* Audio component to play sounds of this block
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UAudioComponent* AudioComponent;

	
public:	
	virtual void BeginPlay();
	virtual void Tick(float DeltaTime);
	float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OnReceiveHit(const FVector& position, const int hitForce);
};
