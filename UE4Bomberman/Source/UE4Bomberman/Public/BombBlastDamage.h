// Juan Belon - 2017

#pragma once

#include "CoreMinimal.h"
#include "Engine/Canvas.h"
#include "GameFramework/DamageType.h"
#include "BombBlastDamage.generated.h"

/**
 * 
 */
UCLASS(const, Blueprintable, BlueprintType)
class UE4BOMBERMAN_API UBombBlastDamage : public UDamageType
{
	GENERATED_BODY()
	
	/** icon displayed in death messages log when killed with this weapon */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	FCanvasIcon KillIcon;

	/** force feedback effect to play on a player hit by this damage type */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UForceFeedbackEffect *HitForceFeedback;

	/** force feedback effect to play on a player killed by this damage type */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UForceFeedbackEffect *KilledForceFeedback;
	
	
};
