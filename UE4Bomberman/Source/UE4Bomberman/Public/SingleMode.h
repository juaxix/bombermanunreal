// Juan Belon - 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4BombermanGameModeBase.h"
#include "SingleMode.generated.h"

/**
 * 
 */
UCLASS()
class UE4BOMBERMAN_API ASingleMode : public AUE4BombermanGameModeBase
{
	GENERATED_BODY()
	
	/** @TODO: After a timeout of no player activity the game will create a procedural level with a bomberman character
	 *   controlled by an AIcontroller
	 */
	
	
};
