// Juan Belon - 2017

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * Class used to control Bomberman Characters from the base c++ class
 * It will disable the input and will walk dropping bombs scanning the map for enemies
 */
UCLASS()
class UE4BOMBERMAN_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
	
	
	
};
