// Juan Belon - 2017

#pragma once

#include "Engine.h"
#include "Components/ActorComponent.h"
#include "LevelGenerator.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UE4BOMBERMAN_API ULevelGenerator : public UActorComponent
{
	GENERATED_BODY()
private:
	bool readLevelFromFile = false;
	TArray<TArray<int>> level_array;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Level")
	float initialCellPositionX = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Level")
	float initialCellPositionY = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Level")
	float initialCellPositionZ = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Level")
	float cellSeparation = 10.7f;

public:	
	// Sets default values for this component's properties
	ULevelGenerator();

protected:
	/**
	* Breakeable Block type that this level generates
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
	UClass* breakeableType;

	/**
	* Non-Breakeable Block type that this level generates
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
	UClass* nonbreakeableType;

	/**
	* Enemies that wander around the level. 
	* @TODO: convert this into an array with all the kind of enemies
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
	UClass* enemyType;

	void InitLevel();
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
