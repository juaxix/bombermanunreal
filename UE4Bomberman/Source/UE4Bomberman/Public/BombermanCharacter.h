// Juan Belon - 2017

#pragma once
#include "Bomb.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BombermanCharacter.generated.h"

//forward powerup
class APowerUp;

UCLASS()
class UE4BOMBERMAN_API ABombermanCharacter : public ACharacter
{
GENERATED_BODY()
private:
	TArray<ABomb*> dropped_bombs;
	

public:
	// Sets default values for this character's properties
	ABombermanCharacter();
	
protected:
	/**
	* Bomb type that this characters drops
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	UClass* bombType;

	/**
	 * Speed of the character
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	float speed = 0.4f;

	/**
	* Current bomb power
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	int bombPower = 1;

	/**
	* Max number of bombs that the character can drop at the same time
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	int maxBombs = 1;

	/**
	* Flag to know if this character is an online representation that replicates from the network
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	bool network_replicatedActor = false;
	
	/**
	* Time that this character bombs take to explode
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	float bombExplosionTimeout = 3.1f;

	/**
	* Color to distinguish this character
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	FColor color;

	/**
	* Name of this character
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	FString displayName;

	/**
	* Name of this character
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	int totalHitPoints = 3;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

	void CollideWithPowerUp(APowerUp* powerup);
	
public:	
	//Input functions
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void Fire();
	void OnMyBombExploded(ABomb *bomb);
	void OnMyBombHitActor(ABomb *bomb, AActor *actor);
	
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	

	UFUNCTION(BlueprintCallable, Category = "Character")
	bool BombsAvailable();



	/**
	 * Let this function to be implementable in Blueprints so each character can
	 * react in a different way 
	 */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Power up")
	void OnPickPowerUp(APowerUp* powerUp);

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
};
