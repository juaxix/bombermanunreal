// Juan Belon - 2017
#pragma once
#include "Engine.h"
#include "UnrealNetwork.h"
#include "Online.h"
#include "Voice.h"
#include "OnlineEngineInterface.h"
#include "OnlineSubsystemUtils.h"
#include "GameFramework/GameModeBase.h"
#include "MultiplayerGameSession.h"
#include "UE4BombermanGameModeBase.h"
#include "MultiplayerGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UE4BOMBERMAN_API AMultiplayerGameMode : public AUE4BombermanGameModeBase
{
	GENERATED_BODY()

	bool useVoipLan = false; //internet or lan
	TSharedPtr<class FOnlineSessionSettings> SessionSettings;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;
	FString masterGameSessionName = "";
	
public:
	AMultiplayerGameMode(const FObjectInitializer& ObjectInitializer);
	
	void OnReceivedGameSessionName(FString sSessionName);
	//Voice chat using unreal networking
	/**
	*	Function to host a game!
	*
	*	@Param		UserID			User that started the request
	*	@Param		SessionName		Name of the Session
	*	@Param		bIsLAN			Is this is LAN Game?
	*	@Param		bIsPresence		"Is the Session to create a presence Session"
	*	@Param		MaxNumPlayers	        Number of Maximum allowed players on this "Session" (Server)
	*/
	bool HostGameSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers);
	/* Delegate called when session created */
	FOnCreateSessionCompleteDelegate OnCreateGameSessionCompleteDelegate;
	/* Delegate called when session started */
	FOnStartSessionCompleteDelegate OnStartGameSessionCompleteDelegate;

	/** Handles to registered delegates for creating/starting a session */
	FDelegateHandle OnCreateGameSessionCompleteDelegateHandle;
	FDelegateHandle OnStartGameSessionCompleteDelegateHandle;
	/**
	*	Function fired when a session create request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	virtual void OnCreateGameSessionComplete(FName SessionName, bool bWasSuccessful);

	/**
	*	Function fired when a session start request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnStartGameSessionOnlineComplete(FName SessionName, bool bWasSuccessful);
	/**
	*	Find an online session
	*
	*	@param UserId user that initiated the request
	*	@param SessionName name of session this search will generate
	*	@param bIsLAN are we searching LAN matches
	*	@param bIsPresence are we searching presence sessions
	*/
	void FindGameSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence);
	/** Delegate for searching for sessions */
	FOnFindSessionsCompleteDelegate OnFindGameSessionCompleteDelegate;

	/** Handle to registered delegate for searching a session */
	FDelegateHandle OnFindGameSessionCompleteDelegateHandle;
	/**
	*	Delegate fired when a session search query has completed
	*
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnFindGameSessionComplete(bool bWasSuccessful);
	/**
	*	Joins a session via a search result
	*
	*	@param SessionName name of session
	*	@param SearchResult Session to join
	*
	*	@return bool true if successful, false otherwise
	*/
	bool JoinGameSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);
	/** Delegate for joining a session */
	FOnJoinSessionCompleteDelegate OnJoinGameSessionCompleteDelegate;

	/** Handle to registered delegate for joining a session */
	FDelegateHandle OnJoinGameSessionCompleteDelegateHandle;
	/**
	*	Delegate fired when a session join request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnJoinGameSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	/** Delegate for destroying a session */
	FOnDestroySessionCompleteDelegate OnDestroyGameSessionCompleteDelegate;

	/** Handle to registered delegate for destroying a session */
	FDelegateHandle OnDestroyGameSessionCompleteDelegateHandle;
	/**
	*	Delegate fired when a destroying an online session has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	virtual void OnDestroyGameSessionComplete(FName SessionName, bool bWasSuccessful);
	
	UFUNCTION(BlueprintImplementableEvent, Category = "VoIP")
	void activateMicro();
};
