// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "BombermanGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4BOMBERMAN_API ABombermanGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
	/**
	 * @TODO: Win conditions:
		o Show win screen when only one player is alive
		o Show a map timer, that counts down and ends the round
		o Show draw when the last players die in the same bomb blast (or chained bombs) or multiple players are alive when the timer runs out
		o After round end, freeze game in its current state
	 * Use a state machine to go between states depending on the GameMode
	 * Reset option from hud end screen
		 o Starts another round
	 */
	
	
};
