// Juan Belon - 2017
#pragma once

#include "Engine.h"
#include "GameFramework/Actor.h"
#include "PowerUp.generated.h"

/**
* \enum 
* The different types of powerups we can have
*/
UENUM(BlueprintType)
enum class EPowerUpTypes : uint8
{
	BLAST_POWER,
	BOMBS_COUNT,
	CHARACTER_SPEED
};


UCLASS()
class UE4BOMBERMAN_API APowerUp : public AActor
{
	GENERATED_BODY()
private:
	bool picked = false;
	/**
	 * Flag to know if it's inside a block
	 */
	bool hidden = true;
	/**
	 * Seconds before the powerup get destroyed after showed up (a breakeable block showed it when it breaks)
	 */
	float timeToHideAfterShowed = 66.0f;
public:	
	// Sets default values for this actor's properties
	APowerUp(const FObjectInitializer& ObjectInitializer);

protected:
	/**
	* Root component of the Block
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	USceneComponent* DefaultSceneRoot;

	/**
	* Contains the mesh and material of this block
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* powerUpMesh;

	/**
	* Audio component to play sounds of this block
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UAudioComponent* AudioComponent;

	/**
	* Type of this powerup
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power up")
	EPowerUpTypes powerUpType;


	/**
	* Speed of rotation of the powerup
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power up")
	float rotationSpeed = 127.0f;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UFUNCTION(BlueprintCallable, Category = "Power up")
	void ShowUp();

	UFUNCTION(BlueprintCallable, Category="Power up")
	void OnPicked();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
