// Juan Belon - 2017

#pragma once
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

//forward declaration of bomberman character class to be used in bomb class
class ABombermanCharacter;

UCLASS()
class UE4BOMBERMAN_API ABomb : public AActor
{
GENERATED_BODY()

private:
	bool exploding = false;
	bool exploded = false;
	bool activated = false;
	float sizeSpeed = -0.007f;
	int power = 1;
	ABombermanCharacter* character_owner;
	float fuseTime = 0.0f;
	float explosionBlastTime = 0.0f;
	float timePerExplosion = 1.2f;
	float timeToEnableCollisionAfterDrop = 0.6f;
	float heartbeatTickTime = 0.015f;
	FTimerHandle FuseTimerHandle;
	FTimerHandle HeartBeatTimerHandle;
	FTimerHandle BlastTimerHandle;
	TArray<AActor*> damagedActors;
public:	
	/**
	* Particle system used to represents fire of the fuse when the bomb is planted
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ParticleSystem)
	UParticleSystem* ps_bombFuseFire;

	/**
	 * Particle system used to propagate fire after the explosion of the bomb
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ParticleSystem)
	UParticleSystem* ps_bombBlastFire;

	/**
	 * First particle system to simulate the explosion of the bomb before the fire propagates
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ParticleSystem)
	UParticleSystem* ps_bombInitialExplosion;


	/** 
	 * Particle system once created (if configured in the blueprint) 
	 */
	UParticleSystemComponent* fuse_fire_component_;

	TArray<UParticleSystemComponent*> fuse_fire_blast_components_;

	// Sets default values for this actor's properties
	ABomb(const FObjectInitializer& ObjectInitializer);
	
	const TArray<AActor*> GetDamagedActors() const { return damagedActors; }
protected:
	/**
	* Root component of the Bomb
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	USceneComponent* DefaultSceneRoot;

	/**
	* Audio component to play sounds of this bomb
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UAudioComponent* AudioComponent;

	/**
	* Contains the mesh and material of the bomb
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")	
	UStaticMeshComponent* bombMesh;

	/**
	* Offset to traslate the fuse fire particle system
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = ParticleSystem)
	FVector fuse_fire_offset = FVector::ZeroVector;
	
	/**
	* Size to scale the fuse fire particle system
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = ParticleSystem)
	FVector fuse_fire_size = FVector::ZeroVector;

	
	
public:	
	
	void HeartBeat();

	float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	virtual void BeginPlay();
	virtual void Tick(float DeltaTime);


	UFUNCTION(BlueprintCallable, Category = "Bomb")
	void Activate(ABombermanCharacter *bombermanOwner, int bombPower,float timeToExplode);

	UFUNCTION(BlueprintCallable, Category="Bomb")
	void Explode();

	UFUNCTION(BlueprintCallable, Category="Bomb")
	bool Exploded();

private:
	void OnBlastsFinished();
};
